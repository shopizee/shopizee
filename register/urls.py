from django.urls import path
from django.contrib.auth import views as auth_views
from register import views
from register.views import UserEditView,PasswordChangeView
from register.forms import ResetPasswordForm
urlpatterns = [
    # path("register",views.register, name='register'),
    path("login",views.login, name='login'),
    path("logout/",views.logout, name='logout'),

    path("my_product/",views.my_blog, name='my_product'),
    path('edit_profile/',UserEditView.as_view(), name='edit_profile'),
    # path('password/',auth_views.PasswordChangeView.as_view(template_name='register/change_password.html')),
    path('password/',PasswordChangeView.as_view(template_name='register/change_password.html'),name='password'),
    
    path('reset_password/',
        auth_views.PasswordResetView.as_view(template_name = 'register/ForgotPassword.html', html_email_template_name='register/password_reset_html_email.html'),
        name="reset_password"),

    path('reset_password_sent/', 
        auth_views.PasswordResetDoneView.as_view(template_name = 'register/PasswordResetDone.html'), 
        name="password_reset_done"),

    path('reset/<uidb64>/<token>/',
        auth_views.PasswordResetConfirmView.as_view(template_name = 'register/PasswordResetConfirm.html'), 
        name="password_reset_confirm"),

    path('reset_password_complete/', 
        auth_views.PasswordResetCompleteView.as_view(template_name = 'register/PasswordResetComplete.html'), 
        name="password_reset_complete"),

    # AJAX URLS
    path("ajax/delete_account/",views.delete_user, name="delete_account"),
    # AJAX URLS  
]
