from django.shortcuts import render,redirect
from django.contrib.auth.models import User, auth
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import UserChangeForm, PasswordChangeForm
from django.contrib.auth.views import PasswordChangeView
from django.views import generic
from register.forms import EditProfileForm, PasswordChangingForm
from django.contrib.admin.views.decorators import staff_member_required
from products.models import Product_Details, ProductImage, Category
from django.urls import reverse_lazy
from django.core.paginator import Paginator
from django.template.loader import render_to_string
from django.http import HttpResponseRedirect,Http404,JsonResponse

# Create your views here.

def login(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']

        user =auth.authenticate(username=username,password=password)

        if user is not None:
            messages.success(request, 'Login Successful')
            auth.login(request, user)
            return redirect("/")
        else:
            messages.info(request,'The username or password you entered is incorrect')
            return redirect('login')
    else:
        if request.user.is_authenticated:
            return redirect('/')
        else:
            return render(request, 'register/login.html')
            

@login_required(login_url='/')
def logout(request):
    messages.success(request, 'You are successfully logged out')
    auth.logout(request)
    return redirect('/')

@staff_member_required(login_url='/')
def my_blog(request):
    product = Product_Details.objects.all()
    bool = False
    if Product_Details.objects.all():
        bool = True
    else:
        bool = False

    paginator = Paginator(product, 25) # Show 4 contacts per page.

    page_number = request.GET.get('page')
    page_obj = paginator.get_page(page_number)

    cat_menu = Category.objects.all()[:5]
    category = Category.objects.all()[:3]

    return render(request, 'register/my_product.html',{'page_obj': page_obj, 'category':category, 'cat_menu' : cat_menu, 'bool':bool})

class UserEditView(generic.UpdateView):
    form_class = EditProfileForm
    template_name = 'register/edit_profile.html'
    success_url = reverse_lazy('index')
    
    def get_object(self):
        return self.request.user

class PasswordChangeView(PasswordChangeView):
    # form_class = PasswordChangingForm
    form_class = PasswordChangingForm
    success_url = reverse_lazy('index')

@login_required(login_url='/')
def delete_user(request):   
    if request.is_ajax and request.method == "POST" and request.user.is_authenticated == True:
        try:
            delete_user = User.objects.get(username = request.user.username)
            delete_user.delete()
            # messages.success(request, "Account Deleted Successfully")
            html = render_to_string('register/edit_profile.html',request=request)
            return JsonResponse({'form':html},status=200)

        except User.DoesNotExist:
            messages.error(request, "User doesnot exist")    
            # html = render_to_string('register/edit_profile.html',request=request)
            # return JsonResponse({'form':html},status=200)
 


