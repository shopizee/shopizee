# Generated by Django 3.2 on 2021-04-15 11:52

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0006_alter_product_details_description'),
    ]

    operations = [
        migrations.AddField(
            model_name='product_details',
            name='slug',
            field=models.SlugField(null=True),
        ),
    ]
