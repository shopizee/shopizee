# Generated by Django 3.2 on 2021-04-15 11:06

import ckeditor.fields
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0005_auto_20210415_1105'),
    ]

    operations = [
        migrations.AlterField(
            model_name='product_details',
            name='description',
            field=ckeditor.fields.RichTextField(blank=True, null=True),
        ),
    ]
