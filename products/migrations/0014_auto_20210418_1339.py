# Generated by Django 3.2 on 2021-04-18 08:09

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0013_alter_product_details_slug'),
    ]

    operations = [
        migrations.RenameField(
            model_name='product_details',
            old_name='buy',
            new_name='brand',
        ),
        migrations.AddField(
            model_name='product_details',
            name='link',
            field=models.CharField(default=django.utils.timezone.now, max_length=500),
            preserve_default=False,
        ),
    ]
