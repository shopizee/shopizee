from django.shortcuts import render
from products.models import Product_Details, ProductImage, Category, Brand, Banner
from django.views.generic import ListView, DetailView, DeleteView, UpdateView, CreateView
from django.core.paginator import Paginator
from django.template.loader import render_to_string
from django.http import HttpResponseRedirect,Http404,JsonResponse
from django.views.generic.list import MultipleObjectMixin
from products.forms import ProductForm, EditForm 
from django.urls import reverse, reverse_lazy
from django.contrib.postgres.search import *
# Create your views here.

class HomeView(ListView):
    model = Product_Details
    template_name = 'product/product.html'

    def get_context_data(self, **kwargs):
        # Context object
        context = super(HomeView, self).get_context_data(**kwargs) 
        # Queryset object
        queryset_list =  Product_Details.objects.all()
        query = self.request.GET.get("q")
        empty = False
        if query:
            queryset_list = Product_Details.objects.annotate(
                search = SearchVector('title','description','content')
                ).filter(search = query).distinct()
            if len(queryset_list) == 0:
                empty = True
        # Paginator object
        paginator = Paginator(queryset_list, 20) # Show 4 contacts per page.
        # Pagination
        page_number = self.request.GET.get('page')
        page_obj = paginator.get_page(page_number)
        # Pagination
        # Context return pagination
        cat_menu = Category.objects.all()[:5]
        brand = Brand.objects.all()[:5]

        context["brand"] = brand 
        context["cat_menu"] = cat_menu 
        context["page_obj"] = page_obj
        context["empty"] = empty
        category = Category.objects.all()[:3]
        context["category"] = category 
        # Context return
        return context   

class ProductDetailView(DetailView,MultipleObjectMixin):
    model = Product_Details
    template_name = 'product/product-detail.html'

    def get_context_data(self, **kwargs ):
        products = Product_Details.objects.filter(id=self.kwargs['pk'])
        images = ProductImage.objects.filter(product=self.kwargs['pk'])

        pro = Product_Details.objects.filter(id=self.kwargs['pk']).values()
        for cat in list(pro):
            category_products= Product_Details.objects.filter(category_id = cat['category_id'])
        
        cat_menu = Category.objects.all()[:5]

        context = super(ProductDetailView, self).get_context_data(object_list='',**kwargs) #object_list=parent_comments,
        context["images"] = images 
        context["products"] = products 
        context["category"] = category_products 
        context["cat_menu"] = cat_menu 
        return context

def CategoryView(request, cat):
    idd = Category.objects.filter(category=cat.replace('-', ' '))
    category_products= Product_Details.objects.filter(category = idd[0])
    paginator = Paginator(category_products, 20) # Show 25 contacts per page.

    page_number = request.GET.get('page')
    page_obj = paginator.get_page(page_number)

    cat_menu = Category.objects.all()[:5]
    if category_products:
        return render(request, 'product/category.html', {'cat':cat.title().replace('-', ' '), 'page_obj':page_obj, 'cat_menu':cat_menu})
    else:
        return render(request, 'other/404.html')

class AddPostView(CreateView):
    model = Product_Details
    form_class = ProductForm
    template_name = 'product/add_product.html'
    # fields = '__all__'
    # fields = ('title','description','content','author','category','likes','comments')
    def form_valid(self, form):
        p = form.save()
        images = self.request.FILES.getlist("more_images")
        for i in images:
            ProductImage.objects.create(product=p, images=i)
        return super().form_valid(form)

class UpdatePostView(UpdateView):
    model = Product_Details
    form_class = EditForm
    template_name = 'product/update_product.html'

class DeletePostView(DeleteView):
    model = Product_Details
    template_name = 'product/delete_product.html'
    success_url = reverse_lazy('product')

class AddCategoryView(CreateView):
    model = Category
    template_name = 'product/add_category.html'
    fields = '__all__'
    
class AddBrandView(CreateView):
    model = Brand
    template_name = 'product/add_brand.html'
    fields = '__all__'

class AddBannerView(CreateView):
    model = Banner
    template_name = 'product/add_banner.html'
    fields = '__all__'