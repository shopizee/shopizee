from django.urls import path,include
from  . import views
from django.conf.urls import url
from .views import HomeView, ProductDetailView, CategoryView, AddCategoryView, AddBannerView, AddBrandView, AddPostView, UpdatePostView, DeletePostView
from django.contrib.admin.views.decorators import staff_member_required
urlpatterns = [
    path('',HomeView.as_view(), name="product"),
    path('register/',include('register.urls')),
    path('product/<int:pk>/<slug:slug>/',ProductDetailView.as_view(), name="product_detail"),
    path('category/<str:cat>/',CategoryView, name='category'),

    # ADMIN ONLY
    path('add_product/',staff_member_required(AddPostView.as_view(),login_url='/'), name='add_product'),
    path('add_category/',staff_member_required(AddCategoryView.as_view(),login_url='/'), name='add_category'),
    path('add_brand/',staff_member_required(AddBrandView.as_view(),login_url='/'), name='add_brand'),
    path('add_banner/',staff_member_required(AddBannerView.as_view(),login_url='/'), name='add_banner'),
    path('article/edit/<int:pk>/',staff_member_required(UpdatePostView.as_view(),login_url='/'), name='update_product'),
    path('article/<int:pk>/delete/',staff_member_required(DeletePostView.as_view(),login_url='/'), name='delete_product'),
    # ADMIN ONLY
]
