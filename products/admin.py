from django.contrib import admin
from . models import Product_Details, Category, ProductImage, Brand, Banner

class ProductImageAdmin(admin.StackedInline):
    model = ProductImage
 
@admin.register(Product_Details)
class Product_DetailsAdmin(admin.ModelAdmin):
    inlines = [ProductImageAdmin]
 
    class Meta:
       model = Product_Details
 
@admin.register(ProductImage)
class ProductImageAdmin(admin.ModelAdmin):
    pass

# Register your models here.
# admin.site.register(Product_Details)
admin.site.register(Category)
admin.site.register(Brand)
admin.site.register(Banner)
# admin.site.register(ProductImage)