from django import forms
from products.models import Product_Details, ProductImage, Category, Brand

# From Category table getting all Category
# choices = Category.objects.all().values_list('category','category')

# choice_list = []

# for item in choices:
#     choice_list.append(item)

# Form for AddPost 
class ProductForm(forms.ModelForm):
    more_images =forms.FileField(required=False, widget=forms.FileInput(attrs={
        'class':'form-control',
        'multiple' : True
        }))
    class Meta:
        model= Product_Details
        fields = ('title','description','content','category','img','link','brand')

        widgets = {
            'title': forms.TextInput(attrs={'class':'form-control'}),
            'description': forms.TextInput(attrs={'class':'form-control'}),
            'content': forms.TextInput(attrs={'class':'form-control'}),
            # 'author': forms.TextInput(attrs={'class':'form-control', 'value':'', 'id':'auth', 'type':'hidden'}),
            # 'author': forms.Select(attrs={'class':'form-control'}),
            'category': forms.Select(attrs={'class':'form-control'}),
            'brand': forms.Select(attrs={'class':'form-control'}),
            'link': forms.TextInput(attrs={'class':'form-control'})
        }

# Form for EditPost 
class EditForm(forms.ModelForm):
    class Meta:
        model= Product_Details
        fields = ('title','description','content','category','img','link','brand')

        widgets = {
            'title': forms.TextInput(attrs={'class':'form-control'}),
            'description': forms.TextInput(attrs={'class':'form-control'}),
            'content': forms.TextInput(attrs={'class':'form-control'}),
            'category': forms.Select(attrs={'class':'form-control'}),
            'brand': forms.Select(attrs={'class':'form-control'}),
            'link': forms.TextInput(attrs={'class':'form-control'})
        }
