from django.db import models
from django.utils import timezone
from ckeditor.fields import RichTextField
# pip install django-ckeditor
from django.template.defaultfilters import slugify
from django.urls import reverse, reverse_lazy
# Create your models here.

class Category(models.Model):
    category = models.CharField(max_length=500, unique=True)

    def __str__(self):
        return self.category

    def get_absolute_url(self):
        return reverse("my_product")

class Brand(models.Model):
    brand = models.CharField(max_length=500, unique=True)

    def __str__(self):
        return self.brand

    def get_absolute_url(self):
        return reverse("my_product")

class Product_Details(models.Model):

    title = models.CharField(max_length=500)
    # description = models.CharField(max_length=100)
    description = RichTextField(blank=True, null=True)
    # img = models.ImageField(upload_to='pics/')
    img = models.FileField(blank=True)
    link = models.CharField(max_length=500)
    # brand = models.CharField(max_length=500)
    offer = models.BooleanField(default=False)
    content = RichTextField(blank=True, null=True)
    # content = models.TextField()
    category = models.ForeignKey(Category,on_delete = models.CASCADE,max_length=500)
    brand = models.ForeignKey(Brand,on_delete = models.CASCADE,max_length=500)
    # slug = models.SlugField(null=True) # new
    date = models.DateTimeField(auto_now_add=True)
    slug = models.SlugField(max_length=500, default=slugify(title), unique=True, null=False)

    class Meta:
        ordering = ['-date']

    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
        self.slug = slugify(self.title)
        super(Product_Details, self).save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse("product")

class ProductImage(models.Model):
    product = models.ForeignKey(Product_Details, default=None, on_delete=models.CASCADE)
    images = models.FileField(upload_to = 'pics/')
 
    def __str__(self):
        return self.product.title

class Banner(models.Model):
    title = models.CharField(max_length=500)
    sub_title = models.CharField(max_length=500)
    link = models.CharField(max_length=500)
    images = models.FileField(upload_to = 'banner/')
