from django.shortcuts import render
from products.models import Product_Details, ProductImage, Category, Brand, Banner
from django.views.generic import ListView, DetailView
from django.core.paginator import Paginator
from django.template.loader import render_to_string
from django.http import HttpResponseRedirect,Http404,JsonResponse
from django.contrib.postgres.search import *

# Create your views here.

class HomeView(ListView):
    model = Product_Details
    template_name = 'shopizee/index.html'

    def get_context_data(self, **kwargs):
        # Context object
        context = super(HomeView, self).get_context_data(**kwargs) 
        # Queryset object
        queryset_list =  Product_Details.objects.all()
        query = self.request.GET.get("q")
        filterr = self.request.GET.get("filterr")
        empty = False
        if query:
            queryset_list = Product_Details.objects.annotate(
                search = SearchVector('title','description','content')
                ).filter(search = query).distinct()
            if len(queryset_list) == 0:
                empty = True

        if filterr:
            idd = Brand.objects.filter(brand = filterr)
            queryset_list =  Product_Details.objects.filter(brand=idd[0])
            if len(queryset_list) == 0:
                empty = True

        # Paginator object
        paginator = Paginator(queryset_list, 20) # Show 4 contacts per page.
        # Pagination
        page_number = self.request.GET.get('page')
        page_obj = paginator.get_page(page_number)
        # Pagination
        # Context return pagination
        context["page_obj"] = page_obj
        # Context Images
        images = ProductImage.objects.all()
        cat_menu = Category.objects.all()[:5]
        brand = Brand.objects.all()[:5]
        banner = Banner.objects.all()

        context["banner"] = banner 
        context["brand"] = brand 
        context["images"] = images 
        category = Category.objects.all()[:3]
        context["category"] = category 
        context["cat_menu"] = cat_menu 
        context["empty"] = empty
        # Context return
        return context

def DetailsView(request, pk):
    if request.is_ajax and request.method == "POST":
        products =  Product_Details.objects.filter(id=pk)
        context = {
            'products':products,
        }
        html = render_to_string('shopizee/details.html',context,request=request)
        return JsonResponse({'form':html,'redirect':True},status=200)

def PrivacyView(request):
    return render(request, "other/privacy.html")

def TermsView(request):
    return render(request, "other/terms.html")

def DisclaimerView(request):
    return render(request, "other/disclaimer.html")

def AboutView(request):
    return render(request, "other/about.html")

def ContactView(request):
    return render(request, "other/contact.html")