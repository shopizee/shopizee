from django.apps import AppConfig


class ShopizeeConfig(AppConfig):
    name = 'Shopizee'
