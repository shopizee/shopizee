"""Shopizee URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path,include
from django.conf.urls.static import static
from django.conf import settings
from . import views
from .views import HomeView, DetailsView
from django.conf.urls import url
# from django.contrib.sitemaps.views import sitemap

urlpatterns = [
    path('admin/', admin.site.urls),
    path('shop/',include('products.urls')),
    path('register/',include('register.urls')),
    path('',HomeView.as_view(), name='index'),
    # path('sitemap.xml', sitemap, {'sitemaps': sitemaps}, name='django.contrib.sitemaps.views.sitemap'),
    # AJAX URLS
    path("ajax/details/<int:pk>/",views.DetailsView, name="details"),
    # AJAX URLS   
    path('privacy-policy/',views.PrivacyView, name='privacy-policy'),
    path('terms/',views.PrivacyView, name='terms'),
    path('disclaimer/',views.DisclaimerView, name='disclaimer'),
    path('about/',views.AboutView, name='about'),
    path('contact/',views.ContactView, name='contact'),
]


urlpatterns = urlpatterns + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)