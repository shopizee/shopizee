var csrf = $("input[name=csrfmiddlewaretoken]").val();
// LIKE AJAX
$(document).on('submit', '.details', function (event) {
   event.preventDefault();
   var frm = $('.details');
   $.ajax({
      type: 'POST',
      url: frm.attr('action'), 
      data: {csrfmiddlewaretoken:csrf},
      dataType: 'json',
      success: function (response) {
            $('.modals').html(response['form']);
            // Ajax End
           }
      }
   )
})
$('.js-show-modal1').on('click',function(e){
   e.preventDefault();
   $('.js-modal1').addClass('show-modal1');
});

$('.js-hide-modal1').on('click',function(){
   $('.js-modal1').removeClass('show-modal1');
});