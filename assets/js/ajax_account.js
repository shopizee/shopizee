$(document).on("submit", ".delete_accounts", function(e){
    $('.modal').removeClass('in');
    $('.modal').attr("aria-hidden","true");
    $('.modal').css("display", "none");
    $('.modal-backdrop').remove();
    $('body').removeClass('modal-open');
 });

 // Delete  
 $(document).on('submit', '.delete_accounts', function (event) {
    event.preventDefault();
    $.ajax({
       type: 'POST',
       url: window.page_data.delete_account_url, 
       data: $(this).serialize(),
       dataType: 'json',
       success: function (response) {
            //Success Modal
            $('#success_modal').modal('show');
            }
       }
    )
 })
 
// Success redirect
 $(document).on('submit', '.success_redirect', function (event) {
   event.preventDefault();
   window.location.href = window.page_data.main_url;
});

 
